package com.heima.article.service;

/**
 * @author VectorX
 * @version V1.0
 * @description
 * @date 2024-05-22 11:24:06
 */
public interface HotArticleService
{

    /**
     * 计算热点文章
     */
    public void computeHotArticle();
}
